var io = require('socket.io');
var http = require('http');
var sys = require('sys');
var net = require('net');

var httpClient = null;



var tcpPipe = net.createServer(function(socket) {
  sys.puts("Connection on tcp pipe");
  socket.on('data', function(msg) {
    if (httpClient) {
      httpClient.send(msg);  
    }
  });
});
tcpPipe.listen(20000, '127.0.0.1');


var httpServer = http.createServer(function(req, res) {
}).listen(8082);

var socket = io.listen(httpServer);
socket.set('log level', 1);
socket.on('connection', function(client) { 
  sys.puts("New http client is here!");
  httpClient = client;
  
  client.on('disconnect', function() { sys.puts("Client has disconnected"); httpClient = null; }) ;
});