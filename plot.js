var apLatencySmoothie = null;
var mLatencySmoothie = null;
var epLatencySmoothie = null;

var apTputSmoothie = null;
var mTputSmoothie = null;
var epTputSmoothie = null;


function init() {
    apLatencySmoothie = new SmoothieChart({ millisPerPixel: 100, grid: { millisPerLine: 5000 } });
    apLatencySmoothie.streamTo(document.getElementById("apLatencyCanvas"));

    mLatencySmoothie = new SmoothieChart({ millisPerPixel: 100, grid: { millisPerLine: 5000 } });
    mLatencySmoothie.streamTo(document.getElementById("mLatencyCanvas"));
    
    epLatencySmoothie = new SmoothieChart({ millisPerPixel: 100, grid: { millisPerLine: 5000 } });
    epLatencySmoothie.streamTo(document.getElementById("epLatencyCanvas"));

    
    apTputSmoothie = new SmoothieChart({ millisPerPixel: 100, grid: { millisPerLine: 5000 } });
    apTputSmoothie.streamTo(document.getElementById("apTputCanvas"));
    
    mTputSmoothie = new SmoothieChart({ millisPerPixel: 100, grid: { millisPerLine: 5000 } });
    mTputSmoothie.streamTo(document.getElementById("mTputCanvas"));
    
    epTputSmoothie = new SmoothieChart({ millisPerPixel: 100, grid: { millisPerLine: 5000 } });
    epTputSmoothie.streamTo(document.getElementById("epTputCanvas"));
    
    
    apQueueSmoothie = new SmoothieChart({ millisPerPixel: 100, grid: { millisPerLine: 5000 } });
    apQueueSmoothie.streamTo(document.getElementById("apQueueCanvas"));
    
    mQueueSmoothie = new SmoothieChart({ millisPerPixel: 100, grid: { millisPerLine: 5000 } });
    mQueueSmoothie.streamTo(document.getElementById("mQueueCanvas"));
    
    epQueueSmoothie = new SmoothieChart({ millisPerPixel: 100, grid: { millisPerLine: 5000 } });
    epQueueSmoothie.streamTo(document.getElementById("epQueueCanvas"));
    connect(); 
}

function connect() {
    var socket = io.connect("<INSERT-YOUR-SERVER-ADDRESS-HERE>:8082");
    socket.on('connect', function() { 
        console.log('connected to server'); 
    });
    
    socket.on('disconnect', function() { 
        console.log('disconnected from server'); 
        connect();
    });
    
    update(socket);
}

function updateTimeSeries(pid, plots, smoothie, x, y) {
    if (plots[pid] == undefined) {
        plots[pid] = new TimeSeries()
        smoothie.addTimeSeries(plots[pid], { strokeStyle: rainbow(Object.keys(plots).length, Object.keys(plots).indexOf(pid)) });
    }
    
    plots[pid].append(x, y);
}

function update(socket) {
    var apLatencyPlots = [];
    var mLatencyPlots = [];
    var epLatencyPlots = [];

    var apTputPlots = [];
    var mTputPlots = [];
    var epTputPlots = [];

    var apQueuePlots = [];
    var mQueuePlots = [];
    var epQueuePlots = [];
    
    
    var apTputDiff = {};
    var mTputDiff = {};
    var epTputDiff = {};
    
    socket.on('message', function(msg) { 
        var lines = msg.split('\n');
        for (var i = 0; i < lines.length - 1; i++) {
            fields = lines[i].split(' ');
            switch (fields[3]) {
            case 'latency':
                switch (fields[1]) {
                case 'ap':
                    updateTimeSeries(fields[2], apLatencyPlots, apLatencySmoothie, new Date().getTime(), fields[4]);
                    break;
                case 'm':
                    updateTimeSeries(fields[2], mLatencyPlots, mLatencySmoothie, new Date().getTime(), fields[4]);
                    break;
                case 'ep':
                    updateTimeSeries(fields[2], epLatencyPlots, epLatencySmoothie, new Date().getTime(), fields[4]);
                    break;
                }
                break;
            case 'messages':
                switch(fields[1]) {
                case 'ap':
                    var x = new Date().getTime();
                    if (apTputDiff[fields[2]] !== undefined) {
                        var dy = (fields[4] - apTputDiff[fields[2]][1]) / ((x - apTputDiff[fields[2]][0]) + 1);
                        updateTimeSeries(fields[2], apTputPlots, apTputSmoothie, x, dy);
                    }
                    apTputDiff[fields[2]] = [x, fields[4]];
                    break;
                case 'm':
                    var x = new Date().getTime();
                    if (mTputDiff[fields[2]] !== undefined) {
                        var dy = (fields[4] - mTputDiff[fields[2]][1]) / ((x - mTputDiff[fields[2]][0]) + 1); 
                        updateTimeSeries(fields[2], mTputPlots, mTputSmoothie, x, dy);
                    }
                    mTputDiff[fields[2]] = [x, fields[4]];
                    break;
                case 'ep':
                    var x = new Date().getTime();
                    if (epTputDiff[fields[2]] !== undefined) {
                        var dy = (fields[4] - epTputDiff[fields[2]][1]) / ((x - epTputDiff[fields[2]][0]) + 1); 
                        updateTimeSeries(fields[2], epTputPlots, epTputSmoothie, x, dy);
                    }
                    epTputDiff[fields[2]] = [x, fields[4]];
                    break;
                }
                break;
            case 'queue_len':
                switch (fields[1]) {
                case 'ap':
                    updateTimeSeries(fields[2], apQueuePlots, apQueueSmoothie, new Date().getTime(), fields[4]);
                    break;
                case 'm':
                    updateTimeSeries(fields[2], mQueuePlots, mQueueSmoothie, new Date().getTime(), fields[4]);
                    break;
                case 'ep':
                    updateTimeSeries(fields[2], epQueuePlots, epQueueSmoothie, new Date().getTime(), fields[4]);
                    break;
                }
                break;
            }
        }
    });
}



function rainbow(numOfSteps, step) {
    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distiguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    var r, g, b;
    var h = step / numOfSteps;
    var i = ~~(h * 6);
    var f = h * 6 - i;
    var q = 1 - f;
    switch(i % 6){
    case 0: r = 1, g = f, b = 0; break;
    case 1: r = q, g = 1, b = 0; break;
    case 2: r = 0, g = 1, b = f; break;
    case 3: r = 0, g = q, b = 1; break;
    case 4: r = f, g = 0, b = 1; break;
    case 5: r = 1, g = 0, b = q; break;
    }
    var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
    return (c);
}
