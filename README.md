Introduction
============

*monGuy* is a simple realtime monitoring framework - heck, it's the monitor guy's choice.
It is completely oblivious to the type of message you need to send/display.

Rationale
---------
Say, you have a remote machine that you want to monitor.
You might have a daemon running there which reads performance data from time to time.
Now, the question is: How do you stream this data into your webbrowser in order to get a live visualization of this data?

This is what *monGuy* does for you: It comes with a simple *node.js*-based relay server (server.js) which opens a tcp-socket and an http-scoket.
Then it simply forwards the data from the tcp-scoket to the http-client - that's it!

Running
-------
Copy _server.js_ to wherever you want to run your realy-server.
It opens a tcp-socket on port 20000 to which you can connect the data-producer.
The http-socket is opened on port 8082.

Example
-------
The example consists of _index.html_, _plot.js_ and _smoothie.js_.

1.  Open index.html in your local browser
    It will connect to the *node.js* server (you probably need to adjust the hardcoded names in _index.html_)
    and download the *socket.io* library.
2.  plot.js will open a web-socket to the *node.js* server and listen for new messages.
    Once new mesages arrive, it parses and appends them to the plots.

In this example, we use *smoothie charts* to display the plots.